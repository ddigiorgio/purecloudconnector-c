﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softphone.PureCloud.Connector.Core.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            PureCloudConnector connector = PureCloudConnectorFacade.Connector;
            connector.Init(6970, 6969);

            connector.IncomingCTIEvent += Connector_IncomingCTIEvent;
            connector.ActivateSession += Connector_ActivateSession;
            connector.Start();

            Console.ReadKey();
            MakeCall("1234455", connector);
            Console.ReadKey();
            connector.Stop();
        }

        private static void Connector_ActivateSession(object sender, EventArgs e)
        {
            Console.WriteLine("SESSION is ACTIVE");            
        }

        private static void Connector_IncomingCTIEvent(object sender, Model.PureCloudEventIncomingArgs e)
        {
            Console.WriteLine("Message: " + e.EventName);
            Console.WriteLine("Message: " + e.EventMessageString);
        }

        private static void MakeCall(string phoneNumber, PureCloudConnector connector)
        {

            IDictionary<string, string> map = new Dictionary<string, string>();
            map.Add("number", phoneNumber);
            
            connector.SendCommand("MAKEACALL", map);
        }
    }
}
