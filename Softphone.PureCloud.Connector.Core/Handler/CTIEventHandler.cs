﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softphone.PureCloud.Connector.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uhttpsharp;

namespace Softphone.PureCloud.Connector.Core.Handler
{
    class CTIEventHandler : IHttpRequestHandler
    {
        private EventHandler<PureCloudEventIncomingArgs> IncomingCTIEvent;
        private EventHandler<EventArgs> ActivateSession;
        private PureCloudConnector _connector;

        public CTIEventHandler(EventHandler<PureCloudEventIncomingArgs> IncomingCTIEvent,
                               EventHandler<EventArgs> ActivateSession,
                               PureCloudConnector conn)
        {
            this.IncomingCTIEvent = IncomingCTIEvent;
            this.ActivateSession = ActivateSession;
            this._connector = conn;
        }


        public Task Handle(IHttpContext context, Func<Task> next)
        {
            string result = System.Text.Encoding.UTF8.GetString(context.Request.Post.Raw);

            JObject postObject = (JObject)JsonConvert.DeserializeObject(result, typeof(JObject));

            string eventName = postObject["CTIEventName"].ToString().ToUpperInvariant();

            switch (eventName)
            {
                case "ACTIVATESESSION":
                    _connector.SetState(true);

                    EventHandler<EventArgs> handlerActivate = this.ActivateSession;
                    if (handlerActivate != null)
                    {
                        handlerActivate.BeginInvoke(this, new PureCloudEventIncomingArgs()
                        {
                            EventName = postObject["CTIEventName"].ToString(),
                            EventMessageString = postObject["Message"].ToString(),
                            EventMessageObject = JsonConvert.DeserializeObject(postObject["Message"].ToString(), typeof(JObject))
                        }, null, null);
                    }
                    break;
                default:
                    EventHandler<PureCloudEventIncomingArgs> handler = this.IncomingCTIEvent;
                    if (handler != null)
                    {                        
                        handler.BeginInvoke(this, new PureCloudEventIncomingArgs() {
                            EventName = postObject["CTIEventName"].ToString(),
                            EventMessageString = postObject["Message"].ToString(),
                            EventMessageObject = JsonConvert.DeserializeObject(postObject["Message"].ToString(), typeof(JObject))
                        }, null, null);
                    }
                    break;
            }

            context.Response = uhttpsharp.HttpResponse.CreateWithMessage(HttpResponseCode.Ok, string.Empty, true);
            return Task.Factory.GetCompleted();
        }


    }
}
