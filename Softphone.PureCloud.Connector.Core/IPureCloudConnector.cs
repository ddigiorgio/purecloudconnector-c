﻿using Softphone.PureCloud.Connector.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softphone.PureCloud.Connector.Core
{
    public interface IPureCloudConnector
    {
        bool IsConnected();

        void Start();

        void Stop();

        void SendCommand(string commandName, dynamic parameters);

        event EventHandler<PureCloudEventIncomingArgs> IncomingCTIEvent;

        event EventHandler<EventArgs> ActivateSession;
    }
}
