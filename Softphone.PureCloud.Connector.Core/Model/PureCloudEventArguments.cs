﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softphone.PureCloud.Connector.Core.Model
{
    public class PureCloudEventIncomingArgs : EventArgs
    {
        public string EventName { get; set; }

        public string EventMessageString { get; set; }

        public dynamic EventMessageObject { get; set; }
    }
}
