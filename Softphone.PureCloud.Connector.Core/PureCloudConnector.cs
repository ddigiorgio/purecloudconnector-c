﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Softphone.PureCloud.Connector.Core.Handler;
using Softphone.PureCloud.Connector.Core.Model;
using uhttpsharp;
using uhttpsharp.Listeners;
using uhttpsharp.RequestProviders;

namespace Softphone.PureCloud.Connector.Core
{
    public class PureCloudConnector : IPureCloudConnector
    {
        private static readonly HttpClient client = new HttpClient();

        private int _receiverPort;
        private int _senderPort;
        private bool _connected = false;

        public event EventHandler<PureCloudEventIncomingArgs> IncomingCTIEvent;
        public event EventHandler<EventArgs> ActivateSession;
        private HttpServer httpServer;

        public PureCloudConnector()
        { 
        }

        internal void SetState(bool _connected)
        {
            this._connected = _connected;
        }

        public void Init(int rPort, int sPort)
        {
            this._receiverPort = rPort;
            this._senderPort = sPort;
        }

        public bool IsConnected()
        {
            return this._connected;
        }

        /*
         * var command = { 'command': 'MAKEACALL', 'sessionid': sessionid, 'number': number, 'attachdata': this.compatibilityMap(params) };
         * */
        public void SendCommand(string commandName, dynamic parameters)
        {
            if (!this.IsConnected() || string.IsNullOrEmpty(commandName))
                return;

            dynamic message = new System.Dynamic.ExpandoObject();
            message.command = commandName;

            if (parameters != null)
            {
                var expandDic = (IDictionary<string, object>)message;
                foreach (string key in parameters.Keys)
                {
                    expandDic.Add(key, parameters[key]);
                }
            }

            string jsonMessage = HttpUtility.UrlEncode(JsonConvert.SerializeObject(message));

            string url = string.Format("http://localhost:{0}/EXECUTECOMMAND?message={1}", this._senderPort, jsonMessage);

            client.GetAsync(new Uri(url));
        }

        public void Start()
        {
            httpServer = new HttpServer(new HttpRequestProvider());
            
            // Normal port 80 :
            httpServer.Use(new TcpListenerAdapter(new TcpListener(IPAddress.Loopback, this._receiverPort)));

            // Ssl Support :
            //var serverCertificate = X509Certificate.CreateFromCertFile(@"TempCert.cer");
            //httpServer.Use(new ListenerSslDecorator(new TcpListenerAdapter(new TcpListener(IPAddress.Loopback, 443)), serverCertificate));

            // Request handling : 
            httpServer.Use((context, next) => {                    
                return next();
            });

            var ctiHandler = new CTIEventHandler(this.IncomingCTIEvent, this.ActivateSession, this);                
            httpServer.Use(ctiHandler);

                
            httpServer.Start();
        }

        public void Stop()
        {
            if (httpServer != null)
                httpServer.Dispose();
        }
    }
}
