﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softphone.PureCloud.Connector.Core
{
    

    public static class PureCloudConnectorFacade
    {
        private static volatile PureCloudConnector _connector;
        private static object syncRoot = new object();

        public static PureCloudConnector Connector
        {
            get
            {
                if (_connector == null)
                {
                    lock(syncRoot)
                    {
                        if (_connector == null)
                            _connector = new PureCloudConnector();
                    }
                }

                return _connector;
            }
        }
    }
}
